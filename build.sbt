import Dependencies._

scalaVersion in ThisBuild := "2.12.4"

lazy val commonSettings = Seq(
  organization := "io.robot",
  version := "0.1.0-SNAPSHOT",
  startYear := Option(2017),
  crossScalaVersions := Seq("2.11.12", "2.12.4"),
  fork := true,
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ywarn-dead-code",
    "-language:implicitConversions",
    "-language:higherKinds",
    "-language:existentials",
    "-language:postfixOps"
  ),
	publishMavenStyle := true,

	resolvers += "aws maven" at "s3://s3-ap-northeast-1.amazonaws.com/dotm2",
	publishTo := Some("aws maven" at "s3://s3-ap-northeast-1.amazonaws.com/dotm2"),
  publishArtifact in(Compile, packageDoc) := false,
  scalacOptions ++= Seq(scalaVersion.value match {
    case x if x.startsWith("2.12.") => "-target:jvm-1.8"
    case x => "-target:jvm-1.7"
  }),
  libraryDependencies ++= Seq(
    Dependencies.scalaTest
  )
)

lazy val domain = (project in file("domain"))
  .settings(commonSettings: _*)

lazy val server = (project in file("server"))
  .settings(commonSettings: _*)
  .dependsOn(core)


lazy val core = (project in file("core"))
  .settings(commonSettings: _*)
  .dependsOn(domain)






