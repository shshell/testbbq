//libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies ++= Seq(
  Dependencies.logback,
  Dependencies.akka,
  Dependencies.jsoup,
  Dependencies.util,
  Dependencies.okHttp,
  Dependencies.ioSocket
)


import java.text.SimpleDateFormat
import java.util.Date

val deployTask = TaskKey[Int]("deploy", "deploy to server!")
deployTask := {
  val log = streams.value.log
  val versionTag =  """git log -1 --format="%h""""
  val output = assembly.value.getAbsolutePath

  val remote = "robo@192.168.100.238:/reproxy"
  log.info(s"deploy $output to $remote")

  val buildTime = new SimpleDateFormat("yyyy.MM.dd-HH.mm.ss").format(new Date())

  s"mkdir deploy".!
  val target = s"deploy/app-$buildTime.jar"

  s"mv $output $target".!
  //s"scp $target 1u1:/app/jar".!


  val cmd = s"scp $target $remote"
  log.info(s"exec: $cmd")
  cmd.!

}