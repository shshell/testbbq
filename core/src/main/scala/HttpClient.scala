package io.robot.bbq

import java.io.File
import java.net.{InetSocketAddress, Proxy}
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl._

import scala.collection.JavaConverters._
import okhttp3._

import scala.util.control.Exception.handling


object HttpClient {
  implicit def parseHeader(rawHeader: String) = {
    val headers = scala.collection.mutable.Map.empty[String, String]

    for(line <- rawHeader.split("\n") if line.trim.nonEmpty) {
      try {
        val index = line.indexOf(":")
        val name = line.substring(0, index).trim()
        val value = line.substring(index + 1).trim()
        headers += name -> value
      } catch {
        case e: Throwable =>
        //          log.info(s"Not valid header: $line")
      }
    }
    headers.toMap
  }

  val allHostnameVerifier = new HostnameVerifier() {
    def verify(urlHostName: String, session: SSLSession) = true
  }

  val allCertsTrusted = Array[TrustManager](new X509TrustManager() {
    def getAcceptedIssuers: Array[X509Certificate] = Array[java.security.cert.X509Certificate]()
    def checkClientTrusted(certs: Array[X509Certificate], authType: String){}
    def checkServerTrusted(certs: Array[X509Certificate], authType: String){}
  })

  val sslContext = SSLContext.getInstance("SSL")
  sslContext.init(null, allCertsTrusted, new java.security.SecureRandom())

  val socketFactory = sslContext.getSocketFactory


  def proxyParser(proxyString: String) = {
    //socks://localhost:1080
    val formatError = s"wrong proxy format: $proxyString Example: socks://localhost:1080"
    val uri = new java.net.URI(proxyString)
    val host = uri.getHost
    val port = uri.getPort
    val proxyType = uri.getScheme.toLowerCase match {
      case m if m.contains("socks") =>
        Proxy.Type.SOCKS
      case "http" =>
        Proxy.Type.HTTP
      case _ =>
        sys.error(formatError)
    }

    val proxy =
      handling(classOf[IllegalArgumentException]) by (_ => sys.error(formatError)) apply
        new Proxy(proxyType, new InetSocketAddress(host,port))

    proxy
  }
}

class HttpClient(headers: Map[String, String] = Map.empty,
                 val proxy: String = "",//socks://localhost:1080
                 val session: String = java.util.UUID.randomUUID().toString,
                 val ignoreCertificate: Boolean = false,
                 val log: Option[org.slf4j.Logger] = None
                ) {
  import HttpClient._
  log.map( l => l.debug(s"start httpclient session: $session"))


  private val clientBuilder = new OkHttpClient.Builder()
    .readTimeout(30, TimeUnit.SECONDS)
    .cookieJar(new CookieJar {
      val cookieStore = new java.util.HashMap[HttpUrl, java.util.List[Cookie]]()

      def saveFromResponse(url: HttpUrl, cookies: java.util.List[Cookie]) = {
        cookieStore.put(url, cookies)
      }

      def loadForRequest(url: HttpUrl) = {
        var cookies = cookieStore.get(url)
        if(cookies == null) {
          cookies = new java.util.ArrayList[Cookie]()
        }
        cookies
      }
    })
    .cache(new Cache(new File("session", session), 10 * 1024 * 1024))


  if(!proxy.trim.isEmpty) {
    val p = proxyParser(proxy.trim)
    clientBuilder.proxy(p)
  }

  if(ignoreCertificate) {
    clientBuilder.sslSocketFactory(sslContext.getSocketFactory, allCertsTrusted(0).asInstanceOf[X509TrustManager])
    clientBuilder.hostnameVerifier(allHostnameVerifier)
  }



  implicit val client =  clientBuilder.build()



  val baseHeader = collection.mutable.Map.empty[String, String]
  baseHeader ++= headers
  def setBaseHeader(headers: Map[String, String]) = {
    baseHeader ++= headers
  }


  def post(url: String, json: String, headers: Map[String, String] = Map.empty) = {
    method(url, "POST", headers, Some(json))
  }

  def get(url: String, headers: Map[String, String] = Map.empty): Response = {
    method(url,"GET",headers, None)
  }

  def method(url: String, mtd: String, headers: Map[String, String] = Map.empty, data: Option[String]): Response = {

    val body = data match {
      case Some(json) =>
        RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json)
      case None =>
        null
    }

    val request = new Request.Builder()
         .url(url)
        .method(mtd, body)
        .headers(Headers.of(baseHeader.asJava))
        .headers(Headers.of(headers.asJava))
      .build()

    client.newCall(request).execute()

  }


}