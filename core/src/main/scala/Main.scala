package io.robot.bbq


import akka.actor.{ActorSystem, Props}
import io.robot.bbq.bwin.Bwin

object Main extends App {
  val log = org.slf4j.LoggerFactory.getLogger("Main:")
  log.info("Hello BBQ!")
  implicit val system = ActorSystem()

  val bwin = new Bwin()
  system.actorOf(Props(classOf[BwinActor], bwin), "bwinActor")
//  val server = new Server(10010)

}
