package io.robot.bbq

import java.io.File

import org.slf4j.Logger

abstract class Market(proxy: String, log: Logger) {
  val marketName : String
  val startUrl: String
  val commonHeader = Map(
    "User-Agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"
  )
  val client = new HttpClient(
    headers = commonHeader,
    proxy = proxy,
    ignoreCertificate = true,
    log = Some(log))

  def saveForDebug(data: String, fileName: String) = {
    val fileDir = s"session/${client.session}/debug"
    new File(fileDir).mkdirs()
    im.xun.util.Misc.stringToFile(data, s"$fileDir/$fileName", log)
  }

  def start: Unit
  def stop: Unit
}