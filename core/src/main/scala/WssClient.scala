package io.robot.bbq

import io.socket.client.{IO, Socket}
import io.socket.emitter.Emitter

class WssClient(url: String) {
  val log = org.slf4j.LoggerFactory.getLogger("WssClient:")
  val endpoint = url.replace("wss://", "https://")
  val socket = {
    IO.socket(endpoint)
  }

  val onConnect = new Emitter.Listener {
    override def call(args: AnyRef*): Unit = {
      log.debug("onConnect")
    }
  }

  def on(event: String) = new Emitter.Listener {
    override def call(args: AnyRef*): Unit = {
      log.debug(s"on $event ${args.mkString}")
    }
  }

  socket.on(Socket.EVENT_CONNECT, onConnect)

  def installEvent(name: String) = {
    socket.on(name, on(name))
  }

  installEvent("o")
  installEvent("login")
  installEvent("user left")
  installEvent("new message")
  installEvent("typing")
  installEvent("stop typing")


  def connect() = {
    log.debug(s"Start connect to $endpoint")
    socket.connect
  }

  def login() = {
    socket.emit("add user", "xxxxxxxx")
  }

  def logout() = {
    socket.off("add user", on("logout"))
  }
}