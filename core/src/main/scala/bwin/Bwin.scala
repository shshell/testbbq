package io.robot.bbq.bwin

import java.io.File

import io.robot.bbq.{HttpClient, Market}
import io.circe.optics.JsonPath._
import org.slf4j.Logger
import scala.concurrent.duration._

class Bwin(proxy: String = "",
           val log: Logger = org.slf4j.LoggerFactory.getLogger("Bwin: "))
  extends Market(proxy, log)
    with BwinInit
    with BwinRefresh
{
  val marketName = "Bwin"
  val pullInterval = 8.seconds
  val startUrl = "https://www.bwin3818.com/en-gb/sport/football"

  def start() = {
    log.debug(s"star: $startUrl")
    val  res = sendInitRequest()
    BwinEvents.handleEventsFromBwinResult(res)
    updateVersion(res.version)
    res
  }

  def stop() = {
    log.info("stopping!")
  }

}