package io.robot.bbq

import akka.actor.Actor
import io.robot.bbq.bwin._

class BwinActor(bwin: Bwin) extends Actor {

  val log = org.slf4j.LoggerFactory.getLogger(s"MarketActor-Bwin:")

  case object TICK
  case object INIT

  override def preStart() = {
    self ! INIT
  }

  implicit val ec = context.system.dispatcher

  def pullTick() = {
    context.system.scheduler.scheduleOnce(bwin.pullInterval, self, TICK)
  }

  def receive = {
    case INIT =>
      log.debug("INIT")
      val res = bwin.start()
      pullTick()

    case TICK =>
      log.debug("TICK")
      bwin.pullUpdate() match {
        case Some(u) =>
          log.debug(u.toString)
          BwinEvents.handleEventsFromBwinResult(u)

        case None =>
      }
      pullTick()

    case msg =>
      log.error(s"Unexpected Message: $msg")

  }

}