package io.robot.bbq.bwin

import io.robot.bbq.domain.bwin.{BwinRefreshResult, Events, Outcomes}

import scala.collection.mutable.ListBuffer

object BwinEvents {
  val log = org.slf4j.LoggerFactory.getLogger("BwinEvents:")
  val eventStore = scala.collection.mutable.Map.empty[Long,Events]

  def dumpOutcome(outcomes: Option[Seq[Outcomes]]) = {
    if(outcomes.isEmpty) "" else {
      outcomes.get.map{ oc =>
        s"[${oc.id} ${oc.description} ${oc.price}"
      }.mkString("-")
    }
  }

  def showEvt(evt: Events) = {
    def odds(outcomes: Seq[Outcomes]) = {

    }
   if(evt.outcomes.nonEmpty) {
     s"${evt.id.get}:${evt.title.getOrElse("")} ${dumpOutcome(evt.outcomes)}"
   } else {
     s"${evt.id.get}:${evt.title.getOrElse("")} "
   }
  }

  def dumpEventStore() = {
    log.debug(s"dumpEventStore : size ${eventStore.size}")
    var i = 1
    for(evt <- eventStore) {
      val es = evt._2
      log.debug(s"dumpEventStore ${i} ${evt._1} -> ${showEvt(es)}")
      i+= 1
    }
  }

  def handleEventsFromBwinResult(res: BwinRefreshResult) = {
    if(res.sports.nonEmpty && res.sports.get.nonEmpty) {
      for{
        sp <- res.sports.get if sp.events.nonEmpty
        _ = handleEvents(sp.events.get)
      } yield ()
    }
  }

  def handleEvents(evts: Seq[Events]) = {
    log.info(s"handler Events ${evts.size}")
    dumpEventStore()
    for(evt <- evts) {
      log.debug(evt.toString)
      if(evt.event_id.nonEmpty) {
        val id = evt.event_id.get
        log.debug(s"handle event_id: $id")
        if(eventStore.contains(id)) {
          log.debug(s"eventStore:${eventStore.size} update")
          val storedEvt = eventStore(id)

          if(evt.outcomes.nonEmpty) {
            val res = if(storedEvt.outcomes.isEmpty) {
              evt.outcomes.get
            } else {
              updateOutcomes(storedEvt.outcomes.get, evt.outcomes.get)
            }
            eventStore(id) = storedEvt.copy(outcomes = Some(res))
            log.info(s"eventStore updated: ${dumpOutcome(storedEvt.outcomes)} => ${dumpOutcome(Some(res))}")
          } else {
            log.debug(s"updateOutcomes: empty entry, update evt: ${evt.outcomes.isEmpty}")
          }

        } else {
          log.info(s"eventStore:${eventStore.size} add new entry: ${showEvt(evt)}")
          eventStore.put(evt.event_id.get, evt)
        }
      } else {
        log.info(s"event id is empty: $evt")
      }
    }
  }

  def updateOutcomes(stored: Seq[Outcomes], updated: Seq[Outcomes]): Seq[Outcomes] = {
    val buffer = ListBuffer.empty[Outcomes]

    for(u1 <- stored) {
      var find = false
      for(u2 <- updated) {
        if(u1.id.getOrElse(1L) == u2.id.getOrElse(2L)) {
          find = true
          val newItem = u1.copy(price = u2.price, price_decimal = u2.price_decimal)
          log.info(s"updateOutcomes: ${u2.price_decimal}")
          buffer += newItem
        }
      }
      if(!find) {
        log.info(s"updateOutcomes: keep ${u1.price_decimal}")
        buffer += u1
      }
    }

    buffer.toList

  }
}