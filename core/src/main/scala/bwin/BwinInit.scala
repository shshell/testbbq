package io.robot.bbq.bwin

import io.robot.bbq.HttpClient
import io.robot.bbq.domain.bwin._
import io.circe.generic._

trait BwinInit { self: Bwin =>



  //invalid json to json
  def extractResult(json: String): BwinRefreshResult = {
    val map = collection.mutable.Map.empty[String, String]
    def parseLine(line: String) = {
      line.split(":", 2).map(_.trim)
    }
    json.lines.toList.filter(_.contains(":")).map{ line =>
      val arr = parseLine(line)
      map += arr(0) -> arr(1)
    }
    val version = map("version")
    val sports =  try {
      Sports.fromJsonSeq(map("sports"))
    } catch {
      case e: Throwable =>
        None
    }
    BwinRefreshResult(version,sports)
  }
  //html -> invalid json
  def parseInitJson(html: String): String = {
    val pat = """(?s).*Live.load.frontpage\(\{(.*?)\}\).*""".r
    pat.findFirstMatchIn(html) match {
      case Some(mat) =>
        if(mat.groupCount >= 1) {
          val res = mat.group(1)
          s"{$res}"
        } else {
          sys.error("parseInitJson: fail, group less than 1!")
        }

      case None =>
        sys.error(s"parseInitJson: fail!")
    }
  }

  def sendInitRequest(): BwinRefreshResult = {
    val html = client.get(startUrl).body().string
//    saveForDebug(html,"step1.html")
    val initJson = parseInitJson(html)
//    saveForDebug(initJson,"initJson.json")

    extractResult(initJson)


  }
}