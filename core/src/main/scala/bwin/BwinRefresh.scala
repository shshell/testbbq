package io.robot.bbq.bwin

import io.circe.Json
import io.robot.bbq.HttpClient
import io.circe.optics.JsonPath._
import io.robot.bbq.domain.bwin.BwinRefreshResult
import io.circe.generic.auto._
import io.circe.syntax._

trait BwinRefresh { self: Bwin =>

  var  version  = "null"

  def updateVersion(v: String) = {
    log.debug(s"update version: $v")
    version = v.replaceAll("\"","")
  }

  def refreshUrl() = {
    s"https://www.bwin3818.com/en-gb/sport/live/refresh?sport_id=100&meeting_ids=&version=$version"
  }

  def parseRefresh(str: String) = {

    val json = io.circe.parser.parse(str).getOrElse(Json.Null)
    val optVersion = root.version.string
    val res = optVersion.getOption(json)

    log.debug(s"parseRefresh: new version: ${res}")
    updateVersion(res.get)

  }


  def pullUpdate(): Option[BwinRefreshResult] = {
    val resp = client.get(refreshUrl, headers = Map("referer" -> startUrl)).body().string()
    io.circe.parser.decode[BwinRefreshResult](resp).toOption.map { res =>
      updateVersion(res.version)
      res
    }
  }

  def startLoop() = {
    while(true) {
      val res = client.get(refreshUrl, headers = Map("referer" -> startUrl)).body().string()
      Thread.sleep(8000)
      parseRefresh(res)
      println(res)
    }
  }
}