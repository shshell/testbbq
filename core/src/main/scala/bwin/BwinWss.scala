package io.robot.bbq.bwin

import io.socket.client.{IO, Socket}
import io.socket.emitter.Emitter

class BwinWss(endpoint: String) {
  val log = org.slf4j.LoggerFactory.getLogger("BwinWss")

//  val endpoint = "wss://realtimedata-mp.betvictor.com:8444/eventbus/422/jy8zm4mt/websocket"

  val socket = IO.socket(endpoint)

  val onConnect = new Emitter.Listener {
    override def call(args: AnyRef*): Unit = {
      log.debug("onConnect")
    }
  }

  def on(event: String) = new Emitter.Listener {
    override def call(args: AnyRef*): Unit = {
      log.debug(s"on $event ${args.mkString}")
    }
  }

  socket.on(Socket.EVENT_CONNECT, onConnect)

  def installEvent(name: String) = {
    socket.on(name, on(name))
  }

  installEvent("ping")

  socket.connect()


}