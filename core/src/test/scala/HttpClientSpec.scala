import java.util.regex.Pattern

import io.robot.bbq._
import HttpClient._
import bwin.Bwin
import io.circe.Json
class HttpClientSpec extends TestContext {
  xit should "get" in {
    val client = new HttpClient(proxy = "socks://localhost:1080")
//    client.setBaseHeader(Map("User-Agent" -> "curl"))

    val rawHeader =
      """
        |Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
        |Accept-Encoding:gzip, deflate
        |Accept-Language:zh-CN,zh;q=0.9,en;q=0.8
        |Cache-Control:no-cache
        |Connection:keep-alive
        |Cookie:__utmc=147301557; __utmz=147301557.1518483809.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utma=147301557.1107329543.1518483809.1518483809.1518488548.2; __utmt=1; __utmb=147301557.1.10.1518488548
        |Pragma:no-cache
        |Upgrade-Insecure-Requests:1
        |User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36
      """.stripMargin

    val json = new org.json.JSONObject()
    json.put("hello", "world")
    val resp = client.post("https://httpbin.org/anything",json.toString, headers = rawHeader)
    val res = resp.body().string()
    println(res)
  }



  xit should "parse" in {
    val str =
      """
        |Live.load.frontpage({
        |     version:           "fdc9e53038c0b8b81487c45968242d96_en-gb_2_16_false",
        |     sports:            [{"id":100,"title":"Football","meetings":[],"is_virtual":false,"events":[{"id":916524300,"is_virtual":false,"outcomes":[{"id":51954032300,"description":"Waterhouse F.C","price_decimal":18.0,"price":"17/1","price_id":17176407495111,"print_order":1,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159030,"race_card_no":0},{"id":51954032500,"description":"Draw","price_decimal":4.1,"price":"31/10","price_id":17176407495109,"print_order":2,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159030,"race_card_no":0},{"id":51954032400,"description":"Harbour View","price_decimal":1.25,"price":"1/4","price_id":17176407495110,"print_order":3,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159030,"race_card_no":0}],"event_id":916524300,"title":"Waterhouse F.C v Harbour View","market_id":110159030,"market_type_id":1,"status_id":1,"score":"1-2","description":null,"start_time":1518485700000,"meeting":"JAM Jamaica Premier League","meeting_id":74474410,"media":null,"american_format":false,"event_type":"GAMEEVENT","pos":0,"home_team":"Waterhouse F.C","away_team":"Harbour View","team_information":true,"home_score":1,"away_score":2,"period_id":235,"status_type":"text","status":"Second Half","total_outcomes":3},{"id":916552700,"is_virtual":false,"outcomes":[{"id":51954240100,"description":"Club America (W)","price_decimal":1.025,"price":"1/40","price_id":17176407310139,"print_order":1,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159905,"race_card_no":0},{"id":51954240300,"description":"Draw","price_decimal":8.5,"price":"15/2","price_id":17176406270128,"print_order":2,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159905,"race_card_no":0},{"id":51954240200,"description":"Monarcas Morelia (W)","price_decimal":36.0,"price":"35/1","price_id":17176407310138,"print_order":3,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159905,"race_card_no":0}],"event_id":916552700,"title":"Club America (W) v Monarcas Morelia (W)","market_id":110159905,"market_type_id":1,"status_id":1,"score":"1-0","description":null,"start_time":1518487200000,"meeting":"MEX Liga MX Femenil (W)","meeting_id":382958310,"media":null,"american_format":false,"event_type":"GAMEEVENT","pos":1,"home_team":"Club America (W)","away_team":"Monarcas Morelia (W)","team_information":true,"home_score":1,"away_score":0,"period_id":235,"status_type":"text","status":"Second Half","total_outcomes":3}],"pos":1}],
        |     main_refresh_url: 'https://www.bwin3818.com/en-gb/sport/live/refresh',
        |     live_url:         '/live/en-gb#:id',
        |     query_params:     'sport_id=100&meeting_ids=',
        |     tabs: true,
        |     styling: 'alternative',
        |     max_number_of_events: 2
        |   });
        |
        | });
        |  setTimeout('$(".sport").show()', 3000);
        |</script>
        |<style>
        |  .sport {
        |    display: none
        |  }
        |</style>
      """.stripMargin

    val pat = """(?s).*Live.load.frontpage\(\{(.*?)\}\).*""".r

    val str1 =  pat.findFirstMatchIn(str).get.group(1)


    val json = s"{$str1}"

    println(json)


  }

  xit should "json" in {
    val str =
      """
        |{
        |     version:           "fdc9e53038c0b8b81487c45968242d96_en-gb_2_16_false",
        |     sports:            [{"id":100,"title":"Football","meetings":[],"is_virtual":false,"events":[{"id":916524300,"is_virtual":false,"outcomes":[{"id":51954032300,"description":"Waterhouse F.C","price_decimal":18.0,"price":"17/1","price_id":17176407495111,"print_order":1,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159030,"race_card_no":0},{"id":51954032500,"description":"Draw","price_decimal":4.1,"price":"31/10","price_id":17176407495109,"print_order":2,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159030,"race_card_no":0},{"id":51954032400,"description":"Harbour View","price_decimal":1.25,"price":"1/4","price_id":17176407495110,"print_order":3,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159030,"race_card_no":0}],"event_id":916524300,"title":"Waterhouse F.C v Harbour View","market_id":110159030,"market_type_id":1,"status_id":1,"score":"1-2","description":null,"start_time":1518485700000,"meeting":"JAM Jamaica Premier League","meeting_id":74474410,"media":null,"american_format":false,"event_type":"GAMEEVENT","pos":0,"home_team":"Waterhouse F.C","away_team":"Harbour View","team_information":true,"home_score":1,"away_score":2,"period_id":235,"status_type":"text","status":"Second Half","total_outcomes":3},{"id":916552700,"is_virtual":false,"outcomes":[{"id":51954240100,"description":"Club America (W)","price_decimal":1.025,"price":"1/40","price_id":17176407310139,"print_order":1,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159905,"race_card_no":0},{"id":51954240300,"description":"Draw","price_decimal":8.5,"price":"15/2","price_id":17176406270128,"print_order":2,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159905,"race_card_no":0},{"id":51954240200,"description":"Monarcas Morelia (W)","price_decimal":36.0,"price":"35/1","price_id":17176407310138,"print_order":3,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110159905,"race_card_no":0}],"event_id":916552700,"title":"Club America (W) v Monarcas Morelia (W)","market_id":110159905,"market_type_id":1,"status_id":1,"score":"1-0","description":null,"start_time":1518487200000,"meeting":"MEX Liga MX Femenil (W)","meeting_id":382958310,"media":null,"american_format":false,"event_type":"GAMEEVENT","pos":1,"home_team":"Club America (W)","away_team":"Monarcas Morelia (W)","team_information":true,"home_score":1,"away_score":0,"period_id":235,"status_type":"text","status":"Second Half","total_outcomes":3}],"pos":1}],
        |     main_refresh_url: 'https://www.bwin3818.com/en-gb/sport/live/refresh',
        |     live_url:         '/live/en-gb#:id',
        |     query_params:     'sport_id=100&meeting_ids=',
        |     tabs: true,
        |     styling: 'alternative',
        |     max_number_of_events: 2
        |   })
      """.stripMargin

    val res = io.circe.parser.parse(str)
    println("json")
    println(res)
  }

  xit should "parse json2" in {
    import io.circe.optics.JsonPath._

    val str =
      """
        |{
        |	"version": "cb83198f3a2c5ebfad06a480e766aef3_en-gb_2_1_false",
        |	"sports": []
        |}
      """.stripMargin

    val json = io.circe.parser.parse(str).getOrElse(Json.Null)
    val optVersion = root.version.string
    val res = optVersion.getOption(json)

    println(res)
  }

  it should "bwin" in {
    println("Bwin test")
    val res = new Bwin().start()
    println(res)
  }

}