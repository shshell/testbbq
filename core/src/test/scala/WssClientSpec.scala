import io.robot.bbq._

class WssClientSpec extends TestContext {
  it should "connect" in {
//    val url = "https://socket-io-chat.now.sh/"
//    val url = "https://web-demo.adaptivecluster.com/ws"
    val url = "wss://realtimedata-mp.betvictor.com:8444/eventbus/684/f5854h22/websocket"
    val wssClient = new WssClient(url)
    wssClient.connect()
    wssClient.login()
    scala.io.StdIn.readLine()
    Thread.sleep(2000)
    val msg = "你好啊"
    val json = new org.json.JSONObject()
    json.put("username", "ADXXX")
    json.put("message", msg)
    wssClient.socket.emit("new message", msg)
    println("OVER")
  }
}