package io.robot.bbq.domain


import io.circe._
import io.circe.generic.extras._
import io.circe.generic.extras.semiauto.{deriveDecoder, deriveEncoder}


// 赔率更新消息的类型定义
object MarketUpdateType {
	val MsgOddsUpdated = "ou"
	val MsgOfferRemove = "or"
	val MsgEventRemove = "er"
}


// MarketUpdateMatch 赛事基本信息
case class MarketUpdateMatch( home: String, away: String, start: Long)

object MarketUpdateMatch extends JsonCompanion[MarketUpdateMatch] {
	implicit val customConfig: Configuration = Configuration.default.withDefaults
	implicit val decoder: Decoder[MarketUpdateMatch] = deriveDecoder
	implicit val encoder: Encoder[MarketUpdateMatch] = deriveEncoder
}



// MarketUpdateOddsUpdated 赔率更新信息
case class MarketUpdateOddsUpdated(offer: String, offerSlot: String, offerLine: Long, odds: Long, updated: Long, option: String)

// MarketUpdateOfferRemoved offer关闭
case class MarketUpdateOfferRemoved(offer: String, offerLine: Long, updated: Long)

// MarketUpdateEventRemoved event关闭
case class MarketUpdateEventRemoved(updated: Long)

// MarketUpdateParam 市场信息更新
case class MarketUpdateParam(bookmaker: String,@JsonKey("match") marketUpdateMatch: MarketUpdateMatch, @JsonKey("type") matchType: String)
