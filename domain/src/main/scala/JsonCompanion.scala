package io.robot.bbq.domain

import io.circe._

trait JsonCompanion[T] {

  implicit def thisToString(from: T)(implicit decoder: Decoder[T],encoder: Encoder[T]): String = toJson(from)

  implicit def stringToThis(str: String)(implicit decoder: Decoder[T],encoder: Encoder[T]): T = fromJson(str)

  def fromJson(str: String)(implicit decoder: Decoder[T],encoder: Encoder[T]): T = {
    io.circe.parser.parse(str) match {
      case Left(err) =>
        throw new Exception(err.getMessage)
      case Right(json) =>
        decoder.decodeJson(json) match {
          case Left(err) =>
            throw new Exception(err.getMessage)
          case Right(t) =>
            t
        }
    }
  }


  def fromJsonSeq(str: String)(implicit decoder: Decoder[T],encoder: Encoder[T]): Seq[T] = {
    val seqDecoder = implicitly[Decoder[Seq[T]]]
    io.circe.parser.parse(str) match {
      case Left(err) =>
        throw new Exception(err.getMessage)
      case Right(json) =>
        seqDecoder.decodeJson(json) match {
          case Left(err) =>
            throw new Exception(err.getMessage)
          case Right(t) =>
            t
        }
    }
  }

  def toJson(obj: T)(implicit decoder: Decoder[T],encoder: Encoder[T]): String = {
    encoder(obj).toString()
  }
}