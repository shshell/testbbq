package io.robot.bbq.domain.bwin
import io.circe.generic.auto._, io.circe.syntax._

case class Events(id: Option[Long],
                  is_virtual: Option[Boolean],
                  outcomes: Option[Seq[Outcomes]],
                  event_id: Option[Long],
                  title: Option[String],
                  market_id: Option[Long],
                  market_type_id: Option[Long],
                  status_id: Option[Long],
                  score: Option[String],
                  description: Option[String],
                  start_time: Option[Long],
                  meeting: Option[String],
                  meeting_id: Option[Long],
                  media: Option[String],
                  american_format: Option[Boolean],
                  event_type: Option[String],
                  pos: Option[Long],
                  home_team: Option[String],
                  away_team: Option[String],
                  team_information: Option[Boolean],
                  home_score: Option[Long],
                  away_score: Option[Long],
                  period_id: Option[Long],
                  status_type: Option[String],
                  status: Option[String],
                  total_outcomes: Option[Long]
                 )


case class Outcomes(id: Option[Long],
                    description: Option[String],
                    price_decimal: Option[Double],
                    price: Option[String],
                    price_id: Option[Long],
                    print_order: Option[Long],
                    opponent_id: Option[String],
                    deduction: Option[Boolean],
                    sp: Option[Boolean],
                    nr: Option[Boolean],
                    market: Option[String],
                    market_id: Option[Long],
                    race_card_no: Option[Long]
                   )


case class Sports(id: Option[Long],
                  title: Option[String],
                  meetings: Option[Array[Option[String]]],
                  is_virtual: Option[Boolean],
                  events: Option[Seq[Events]],
                  pos: Option[Option[Long]]
                 )
object Sports {
  def fromJson(json: String): Option[Sports] = {
    io.circe.parser.decode[Sports](json).toOption
  }

  def fromJsonSeq(json: String): Option[Seq[Sports]] = {
    io.circe.parser.decode[Seq[Sports]](json).toOption
  }
}

case class BwinRefreshResult(version:String,
                             sports: Option[Seq[Sports]]
                            )

object BwinRefreshResult {
  def fromJson(json: String): Option[BwinRefreshResult] = {
    io.circe.parser.decode[BwinRefreshResult](json).toOption
  }
}



