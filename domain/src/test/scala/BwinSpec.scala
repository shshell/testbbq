import io.circe.Json
import io.robot.bbq.domain.bwin._
import io.circe.generic.auto._
import io.circe.syntax._

class BwinSpec extends TestContext {
  val json =
    """
      |{
      |	"version": "942a68e4607e79f465313752e9c8ff28_en-gb_2_1_false",
      |	"sports": [{
      |		"id": 100,
      |		"title": "Football",
      |		"meetings": [],
      |		"is_virtual": false,
      |		"events": [{
      |			"id": 914666400,
      |			"is_virtual": false,
      |			"outcomes": [{
      |				"id": 51953388500,
      |				"description": "Huracan BA",
      |				"price_decimal": 5.5,
      |				"price": "9/2",
      |				"price_id": 17176386545104,
      |				"print_order": 1,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110156220,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51953388700,
      |				"description": "Draw",
      |				"price_decimal": 3.75,
      |				"price": "11/4",
      |				"price_id": 17176386480132,
      |				"print_order": 2,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110156220,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51953388600,
      |				"description": "Rosario Central",
      |				"price_decimal": 1.6,
      |				"price": "3/5",
      |				"price_id": 17176386480133,
      |				"print_order": 3,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110156220,
      |				"race_card_no": 0
      |			}],
      |			"event_id": 914666400,
      |			"title": "Huracan BA v Rosario Central",
      |			"market_id": 110156220,
      |			"market_type_id": 1,
      |			"status_id": 1,
      |			"score": "0-1",
      |			"description": null,
      |			"start_time": 1518480900000,
      |			"meeting": "ARG Primera Division",
      |			"meeting_id": 6029210,
      |			"media": null,
      |			"american_format": false,
      |			"event_type": "GAMEEVENT",
      |			"pos": 0,
      |			"home_team": "Huracan BA",
      |			"away_team": "Rosario Central",
      |			"team_information": true,
      |			"home_score": 0,
      |			"away_score": 1,
      |			"period_id": 234,
      |			"status_type": "text",
      |			"status": "First Half",
      |			"total_outcomes": 3
      |		}, {
      |			"id": 916251500,
      |			"is_virtual": false,
      |			"outcomes": [{
      |				"id": 51952581200,
      |				"description": "Talleres Remedios",
      |				"price_decimal": 1.1428571428571428,
      |				"price": "1/7",
      |				"price_id": 17176386730116,
      |				"print_order": 1,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154475,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952581400,
      |				"description": "Draw",
      |				"price_decimal": 5.4,
      |				"price": "22/5",
      |				"price_id": 17176386730114,
      |				"print_order": 2,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154475,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952581300,
      |				"description": "UAI Urquiza",
      |				"price_decimal": 23.0,
      |				"price": "22/1",
      |				"price_id": 17176386760110,
      |				"print_order": 3,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154475,
      |				"race_card_no": 0
      |			}],
      |			"event_id": 916251500,
      |			"title": "Talleres Remedios v UAI Urquiza",
      |			"market_id": 110154475,
      |			"market_type_id": 1,
      |			"status_id": 1,
      |			"score": "1-0",
      |			"description": null,
      |			"start_time": 1518476400000,
      |			"meeting": "ARG Primera B Metropolitana",
      |			"meeting_id": 23223410,
      |			"media": null,
      |			"american_format": false,
      |			"event_type": "GAMEEVENT",
      |			"pos": 1,
      |			"home_team": "Talleres Remedios",
      |			"away_team": "UAI Urquiza",
      |			"team_information": true,
      |			"home_score": 1,
      |			"away_score": 0,
      |			"period_id": 235,
      |			"status_type": "text",
      |			"status": "Second Half",
      |			"total_outcomes": 3
      |		}, {
      |			"id": 914970400,
      |			"is_virtual": false,
      |			"outcomes": [{
      |				"id": 51952648600,
      |				"description": "O'Higgins",
      |				"price_decimal": 34.0,
      |				"price": "33/1",
      |				"price_id": 17176386840106,
      |				"print_order": 1,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154682,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952649000,
      |				"description": "Draw",
      |				"price_decimal": 10.5,
      |				"price": "19/2",
      |				"price_id": 17176386840104,
      |				"print_order": 2,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154682,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952648800,
      |				"description": "Everton CD",
      |				"price_decimal": 1.0454545454545454,
      |				"price": "1/22",
      |				"price_id": 17176386840105,
      |				"print_order": 3,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154682,
      |				"race_card_no": 0
      |			}],
      |			"event_id": 914970400,
      |			"title": "O'Higgins v Everton CD",
      |			"market_id": 110154682,
      |			"market_type_id": 1,
      |			"status_id": 1,
      |			"score": "0-2",
      |			"description": null,
      |			"start_time": 1518476400000,
      |			"meeting": "CHI Primera",
      |			"meeting_id": 5795910,
      |			"media": null,
      |			"american_format": false,
      |			"event_type": "GAMEEVENT",
      |			"pos": 2,
      |			"home_team": "O'Higgins",
      |			"away_team": "Everton CD",
      |			"team_information": true,
      |			"home_score": 0,
      |			"away_score": 2,
      |			"period_id": 235,
      |			"status_type": "text",
      |			"status": "Second Half",
      |			"total_outcomes": 3
      |		}, {
      |			"id": 916486800,
      |			"is_virtual": false,
      |			"outcomes": [{
      |				"id": 51952630000,
      |				"description": "Rangers De Talca",
      |				"price_decimal": 4.5,
      |				"price": "7/2",
      |				"price_id": 17176386780103,
      |				"print_order": 1,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154631,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952630200,
      |				"description": "Draw",
      |				"price_decimal": 1.5,
      |				"price": "1/2",
      |				"price_id": 17176386710123,
      |				"print_order": 2,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154631,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952630100,
      |				"description": "Nublense",
      |				"price_decimal": 5.0,
      |				"price": "4/1",
      |				"price_id": 17176386780102,
      |				"print_order": 3,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154631,
      |				"race_card_no": 0
      |			}],
      |			"event_id": 916486800,
      |			"title": "Rangers De Talca v Nublense",
      |			"market_id": 110154631,
      |			"market_type_id": 1,
      |			"status_id": 1,
      |			"score": "0-0",
      |			"description": null,
      |			"start_time": 1518476400000,
      |			"meeting": "CHI Primera B",
      |			"meeting_id": 17397810,
      |			"media": null,
      |			"american_format": false,
      |			"event_type": "GAMEEVENT",
      |			"pos": 3,
      |			"home_team": "Rangers De Talca",
      |			"away_team": "Nublense",
      |			"team_information": true,
      |			"home_score": 0,
      |			"away_score": 0,
      |			"period_id": 235,
      |			"status_type": "text",
      |			"status": "Second Half",
      |			"total_outcomes": 3
      |		}, {
      |			"id": 916484800,
      |			"is_virtual": false,
      |			"outcomes": [{
      |				"id": 51952563300,
      |				"description": "Real Cartagena",
      |				"price_decimal": 3.7,
      |				"price": "27/10",
      |				"price_id": 17176386830129,
      |				"print_order": 1,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154425,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952563500,
      |				"description": "Draw",
      |				"price_decimal": 1.5,
      |				"price": "1/2",
      |				"price_id": 17176386830127,
      |				"print_order": 2,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154425,
      |				"race_card_no": 0
      |			}, {
      |				"id": 51952563400,
      |				"description": "Orsomarso Clube Sportivo",
      |				"price_decimal": 6.25,
      |				"price": "21/4",
      |				"price_id": 17176386830128,
      |				"print_order": 3,
      |				"opponent_id": null,
      |				"deduction": false,
      |				"sp": false,
      |				"nr": false,
      |				"market": "Match Betting - 90 Mins",
      |				"market_id": 110154425,
      |				"race_card_no": 0
      |			}],
      |			"event_id": 916484800,
      |			"title": "Real Cartagena v Orsomarso Clube Sportivo",
      |			"market_id": 110154425,
      |			"market_type_id": 1,
      |			"status_id": 1,
      |			"score": "0-0",
      |			"description": null,
      |			"start_time": 1518476400000,
      |			"meeting": "COL Primera B",
      |			"meeting_id": 26730310,
      |			"media": null,
      |			"american_format": false,
      |			"event_type": "GAMEEVENT",
      |			"pos": 4,
      |			"home_team": "Real Cartagena",
      |			"away_team": "Orsomarso Clube Sportivo",
      |			"team_information": true,
      |			"home_score": 0,
      |			"away_score": 0,
      |			"period_id": 235,
      |			"status_type": "text",
      |			"status": "Second Half",
      |			"total_outcomes": 3
      |		}],
      |		"pos": 1
      |	}]
      |}
    """.stripMargin
  it should "parse refresh" in {


    val res = io.circe.parser.decode[BwinRefreshResult](json)
    println(res)
    res match {
      case Right(r) =>
//        println("XXXXXXXXXXXXXXXXXXXX")
//        println(r.asJson)
//        println("XXXXXXXXXXXXXXXXXXXX")
      case _ =>
    }

  }

  val updateJson =
    """
      |{
      |	"version": "29ebf4ebdc45f0e884f9c56da4d4792d_en-gb_2_1_false",
      |	"sports": [{
      |		"id": 100,
      |		"pos": 1,
      |		"events": [{
      |			"id": 914666400,
      |			"event_id": 914666400,
      |			"meeting_id": 6029210
      |		}, {
      |			"id": 916251500,
      |			"event_id": 916251500,
      |			"meeting_id": 23223410,
      |			"outcomes": [{
      |				"id": 51952581200
      |			}, {
      |				"id": 51952581400
      |			}, {
      |				"id": 51952581300,
      |				"price_decimal": 26.0,
      |				"price": "25/1",
      |				"price_id": 17176386845131
      |			}]
      |		}, {
      |			"id": 914970400,
      |			"event_id": 914970400,
      |			"meeting_id": 5795910
      |		}, {
      |			"id": 916486800,
      |			"event_id": 916486800,
      |			"meeting_id": 17397810
      |		}, {
      |			"id": 916484800,
      |			"event_id": 916484800,
      |			"meeting_id": 26730310
      |		}]
      |	}]
      |}
    """.stripMargin

//  val rj = """{"version":"a5cb8c05bbbdea80383cacac9116e2ce_en-gb_2_1_false","same":true}"""
  val rj = """{"version":"8c1d87e905a6d009da7a10d25a1ef16b_en-gb_2_1_false","sports":[{"id":100,"title":"Football","meetings":[],"is_virtual":false,"events":[{"id":916553500,"is_virtual":false,"outcomes":[{"id":51955668600,"description":"Bali United Pusam","price_decimal":17.0,"price":"16/1","price_id":17176428180102,"print_order":1,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110163959,"race_card_no":0},{"id":51955668700,"description":"Draw","price_decimal":8.5,"price":"15/2","price_id":17176428180100,"print_order":2,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110163959,"race_card_no":0},{"id":51955668800,"description":"Burma Yangon Utd","price_decimal":1.0625,"price":"1/16","price_id":17176428180101,"print_order":3,"opponent_id":null,"deduction":false,"sp":false,"nr":false,"market":"Match Betting - 90 Mins","market_id":110163959,"race_card_no":0}],"event_id":916553500,"title":"Bali United Pusam v Burma Yangon Utd","market_id":110163959,"market_type_id":1,"status_id":1,"score":"0-3","description":null,"start_time":1518507000000,"meeting":"AFC Cup","meeting_id":12775410,"media":null,"american_format":false,"event_type":"GAMEEVENT","pos":0,"home_team":"Bali United Pusam","away_team":"Burma Yangon Utd","team_information":true,"home_score":0,"away_score":3,"period_id":234,"status_type":"text","status":"First Half","total_outcomes":3}],"pos":1}]}"""
  it should "parse update" in {

    implicit val config = io.circe.generic.extras.Configuration.default.copy(useDefaults = true)
    val res = io.circe.parser.decode[BwinRefreshResult](rj)
    println(res)
  }
}