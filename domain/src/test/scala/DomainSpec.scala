import java.security.cert.X509Certificate

import io.robot.bbq.domain._
import java.security.cert.CertificateFactory
import java.io._

class DomainSpec extends TestContext {
  it should "parse" in {
    val a = MarketUpdateMatch("a","b",0L)
    val res = MarketUpdateMatch.toJson(a)
    println(res)
  }

  it should "test" in {

    val fact = CertificateFactory.getInstance("X.509")
//    val is = new FileInputStream("test.pem")
    val pem =
      """-----BEGIN CERTIFICATE-----
        |MIIFQjCCBCqgAwIBAgIGAWErd8ElMA0GCSqGSIb3DQEBCwUAMIGlMTYwNAYDVQQD
        |DC1DaGFybGVzIFByb3h5IENBICgyNSDkuIDmnIggMjAxOCwgd3VoeC5sb2NhbCkx
        |JTAjBgNVBAsMHGh0dHBzOi8vY2hhcmxlc3Byb3h5LmNvbS9zc2wxETAPBgNVBAoM
        |CFhLNzIgTHRkMREwDwYDVQQHDAhBdWNrbGFuZDERMA8GA1UECAwIQXVja2xhbmQx
        |CzAJBgNVBAYTAk5aMB4XDTAwMDEwMTAwMDAwMFoXDTQ3MDMyNDAzNTg0NFowgaUx
        |NjA0BgNVBAMMLUNoYXJsZXMgUHJveHkgQ0EgKDI1IOS4gOaciCAyMDE4LCB3dWh4
        |LmxvY2FsKTElMCMGA1UECwwcaHR0cHM6Ly9jaGFybGVzcHJveHkuY29tL3NzbDER
        |MA8GA1UECgwIWEs3MiBMdGQxETAPBgNVBAcMCEF1Y2tsYW5kMREwDwYDVQQIDAhB
        |dWNrbGFuZDELMAkGA1UEBhMCTlowggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
        |AoIBAQCTtJaLzYBFgquVtFwLT9SYLNfn/i3PORp1FpxdSCMnrXezORUYvVRCdbuk
        |HMsoComTOUoeCBKi+kjA+jxPNjWIkVt7rFAC/S22LwPKfbIZxcGlrAxZdV0FhSHC
        |TnZ1l5PN5YWDbP1N15GoR7utg4hyKDIfkie26+0S6fKuuPuPe2tfmMDjpa0VdG0D
        |RH34llBNHO5lJ5NR1Vuajnf/qd/2sktabeIx+v5fJpD1WyFEblZqdkvEgs9SHH3X
        |dnwfYXTc/gg7eK8L7bFAxFRfmYZuLTwPNvs3wmU+bproxOwyYDdq0Yx4NfU3TNko
        |pOe0avmCHuDmWg4md+TgmjBx+mnRAgMBAAGjggF0MIIBcDAPBgNVHRMBAf8EBTAD
        |AQH/MIIBLAYJYIZIAYb4QgENBIIBHROCARlUaGlzIFJvb3QgY2VydGlmaWNhdGUg
        |d2FzIGdlbmVyYXRlZCBieSBDaGFybGVzIFByb3h5IGZvciBTU0wgUHJveHlpbmcu
        |IElmIHRoaXMgY2VydGlmaWNhdGUgaXMgcGFydCBvZiBhIGNlcnRpZmljYXRlIGNo
        |YWluLCB0aGlzIG1lYW5zIHRoYXQgeW91J3JlIGJyb3dzaW5nIHRocm91Z2ggQ2hh
        |cmxlcyBQcm94eSB3aXRoIFNTTCBQcm94eWluZyBlbmFibGVkIGZvciB0aGlzIHdl
        |YnNpdGUuIFBsZWFzZSBzZWUgaHR0cDovL2NoYXJsZXNwcm94eS5jb20vc3NsIGZv
        |ciBtb3JlIGluZm9ybWF0aW9uLjAOBgNVHQ8BAf8EBAMCAgQwHQYDVR0OBBYEFNx6
        |uXPVsWEinP7LDAj6OnxBNwReMA0GCSqGSIb3DQEBCwUAA4IBAQCOC5SR5O88AvKf
        |ik3huJaNZAUz40s3ojSDhs0sxLNcd4q8nF9PPQMwvui9jQV6B8/rNHQDRdKgn1Hc
        |5QEEZucefzF+RtBAQPgk54FP04hyCxkzspAowurGaCPpArGHec8NiEPs4PxjFZaY
        |hPcqCgv4oFSdM1ImmCyZ/C/JPIKpfIp8cqXA3duvI1exXmvW0lIXUp0Uiu5+nILs
        |Pps5gBSdfVQtqhcDZNXxPYHHB4yWGCz2z24yxb34fserXY1XhME5BCgvdlJmRYOB
        |bIj2utxYLIP/Z/E/hyJblKc/ltkNHohteNVJBBcFcZGwj9c5y+acMSxBRPPb8D1U
        |BS3SUBg9
        |-----END CERTIFICATE-----
      """.stripMargin
    val is =  new ByteArrayInputStream(pem.getBytes)
    val cer = fact.generateCertificate(is).asInstanceOf[X509Certificate]
    val  key = cer.getPublicKey()
    println(s"XXXXZ: $key")

  }

}