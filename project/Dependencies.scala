import sbt._

object Dependencies {
  val akkaHttpVersion = "10.1.0-RC1"
  val akkaVersion = "2.5.9"

  lazy val scalaTest    = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val logback      = "ch.qos.logback" % "logback-classic" % "1.2.3"
  lazy val akka         =  "com.typesafe.akka" %% "akka-actor" % akkaVersion
  lazy val akkaTest     = "com.typesafe.akka" %% "akka-testkit" % akkaVersion
  lazy val akkaHttp     = "com.typesafe.akka" %% "akka-http" % akkaHttpVersion
  lazy val akkaStream   = "com.typesafe.akka" %% "akka-stream" % akkaVersion
  lazy val akkaHttpTest =  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion
  lazy val browsermob   = "net.lightbody.bmp" % "browsermob-proxy" % "2.1.5"

  lazy val jsoup        = "org.jsoup" % "jsoup" % "1.11.2"
  lazy val dnsjava      = "dnsjava" % "dnsjava" % "2.1.8"
  lazy val util         =   "im.xun" %% "util" % "0.1.1"
  lazy val circeConfig  =  "io.circe" %% "circe-config" % "0.4.0"
  lazy val okHttp       = "com.squareup.okhttp3" % "okhttp" % "3.9.1"
  lazy val okHttpMockwebserver       = "com.squareup.okhttp3" % "mockwebserver" % "3.9.1"
  lazy val ioSocket     = "io.socket" % "socket.io-client" % "1.0.0" // exclude("org.json", "json")

  val googleBintray = Resolver.bintrayRepo("google", "webrtc")
  lazy val webrtc       =  "org.webrtc" % "google-webrtc" % "1.0.21770"
  //libraryDependencies += "org.webrtc" % "google-webrtc" % "1.0.20371"

}
