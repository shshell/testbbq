package io.robot.bbq.server

import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.stream.Materializer
import akka.util.Timeout

import scala.concurrent.Future
import scala.concurrent.duration._
import CirceSupport._
import akka.http.scaladsl.model.HttpResponse

import scala.util.{Failure, Success}

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import StatusCodes._
import Directives._


class ApiRoute()(implicit mat: Materializer) {

  //  implicit val timeout = Timeout(5.seconds)

//  val exceptionHandler = ExceptionHandler {
//    case e: Exception =>
//      extractUri { uri =>
//        val err = s"Request to $uri could not be handled normally: ${e.getMessage}"
//        complete(HttpResponse(InternalServerError, entity = err))
//      }
//  }
//
//
//  def versioned(r: Route): Route = handleExceptions(exceptionHandler) {
//    pathPrefix("v1") {
//      r
//    }
//  }
//
//  val showVpn: Route = {
//    get {
//      pathPrefix("vpn" / IntNumber) { id =>
//        onSuccess {
//          actor.ask(CmdVpnInfo(id))(Timeout(10.seconds)).mapTo[ProxyResponse]
//        } { resp =>
//          complete(resp)
//        }
//      }
//    }
//
//  }
//
//  val createVpn: Route = {
//    path("vpn") {
//      post {
//        entity(as[Vpn]) { vpn =>
//          onComplete {
//            actor.ask(CmdVpnCreate(vpn))(Timeout(10.seconds)).mapTo[ProxyResponse]
//          } {
//            case Success(resp) => complete(resp)
//            case Failure(ex) => complete((InternalServerError, s"An error occurred: ${ex.getMessage}"))
//          }
//        }
//      }
//    }
//  }
//
//  val listVpn: Route = {
//    path("vpn") {
//      get {
//        onSuccess {
//          actor.ask(CmdVpnList)(Timeout(10.seconds)).mapTo[ProxyResponse]
//        } { resp =>
//          complete(resp)
//        }
//      }
//    }
//  }
//
//
//  val deleteVpn: Route = {
//    delete {
//      pathPrefix("vpn" / IntNumber) { id =>
//        onSuccess {
//          actor.ask(CmdVpnDelete(id))(Timeout(10.seconds)).mapTo[ProxyResponse]
//        } { resp =>
//          complete(resp)
//        }
//      }
//    }
//  }
//
//  val task: Route = {
//    post {
//      pathPrefix("vpn" / IntNumber) {id =>
//        entity(as[TaskInfo]) { task =>
//          onSuccess {
//            actor.ask(CmdVpnTask(id,task))(Timeout(10.seconds)).mapTo[ProxyResponse]
//          } { resp =>
//            complete(resp)
//          }
//        }
//      }
//    }
//  }

//  val route = versioned(listVpn ~ createVpn ~ showVpn ~ deleteVpn ~ task)
  val route = post {
    complete("OK")
  }

  //  val route: Route =
  ////    pathPrefix("v1" / "vpn" / IntNumber) {
  //    get {
  //      val fut: Future[String] = vpnSupervisorActor.ask("hello")(Timeout(5.seconds)).mapTo[String]
  ////      complete("x")
  //
  //      onSuccess(fut) { extraction =>
  //        complete(extraction)
  //      }
  //    }
  //  }
}