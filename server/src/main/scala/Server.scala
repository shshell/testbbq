package io.robot.bbq.server

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer


class Server(port: Int) {
  // needed to run the route
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

//  val vpnSupervisorActor = system.actorOf(Props[VpnSupervisorActor], "vpnSupervisorActor")

  val api = new ApiRoute()

//  val port = scala.util.Properties.envOrElse("reproxy_port", "6666").toInt

  val bindingFuture = Http().bindAndHandle(api.route, "0.0.0.0", port)


}